@extends('layouts.admin')

@section('admincontent')
    
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="flex-wrap">
                    <div class="card">
                        {{-- <div class="card-header">{{ __('Register') }}</div> --}}
                        <div class="card-header">
                            <a href="/">
                                <img src="/images/icon/logo-mini.png" alt="Solar Security">
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="card-title">
                                <h3 class="text-center title-2">Add solar installation</h3>
                            </div>
                            <form method="POST" action="{{ route('solarinstallations.store') }}" enctype="multipart/form-data">
                                @csrf

                                @forminputfields(['fieldname' => 'name', 'fieldtype' => 'text', 'attributes' => 'required autofocus'])

                                @forminputfields(['fieldname' => 'location_description', 'fieldlabel' => 'Location description', 'fieldtype' => 'textarea', 'attributes' => 'cols="30" rows="3"'])

                                @forminputfields(['fieldname' => 'latitude', 'fieldtype' => 'text'])

                                @forminputfields(['fieldname' => 'longitude', 'fieldtype' => 'text'])
                                
                                @forminputfields(['fieldname' => 'picture', 'fieldtype' => 'image'])

                                @forminputfields(['fieldtype' => 'submit', 'fieldlabel' => 'Add solar installation'])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

@endsection
