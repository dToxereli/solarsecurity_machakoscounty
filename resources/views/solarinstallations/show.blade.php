@extends('layouts.admin')

@section('admincontent')
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                        <div class="flex-wrap h-100">
                            <div class="card h-100">
                                <img class="card-img-top" src="{{Storage::url($data->picture)}}" alt="Solar installation {{$data->name}}">
                                <div class="card-body">
                                    <h6 class="card-title mb-3 title-2">
                                        {{$data->name}}
                                        @if ($data->alert_status)
                                            <span class="badge badge-danger pull-right" style="font-size: 13px;">
                                                <i class="zmdi zmdi-alert-octagon"></i>
                                                ALARM RAISED
                                            </span>
                                        @else
                                            <span class="badge badge-success pull-right" style="font-size: 13px;">
                                                <i class="zmdi zmdi-lock"></i>
                                                SECURE
                                            </span>
                                        @endif
                                    </h6>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <a class="btn btn-block btn-info" href="{{route('solarinstallations.edit',$data->id)}}">Edit</a>
                                        </div>
                                        <div class="col-sm-6">
                                            @if ($data->alert_status)
                                                <form action="{{route('solarinstallations.secure', $data->id)}}" method="post">
                                                    @csrf
                                                    <button type="submit" class="btn btn-block btn-danger">SECURE</button>
                                                </form>
                                            @else
                                                <button class="btn btn-block btn-danger" disabled="disabled">ALARM OFF</button>
                                            @endif
                                        </div>
                                    </div>
                                    <p class="card-text m-t-20">
                                        {{$data->location_description}}
                                    </p>
                                </div>
                                <div class="card-footer">
                                    <a href="{{$data->google_map_url}}" class="btn btn-block btn-light" target="_blank">
                                        <i class="zmdi zmdi-google-maps"></i>&nbsp; View on google map
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="top-campaign">
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <h3 class="title-3 m-b-30">Alert contacts</h3>
                                </div>
                                <div class="table-data__tool-right">
                                    <a href="{{route('alertcontacts.index')}}" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i class="zmdi zmdi-plus"></i>add alert contact
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-top-campaign">
                                    <tbody>
                                        @foreach ($data->alert_contacts as $alert_contact)
                                            <tr>
                                                <td>{{$alert_contact->name}}</td>
                                                <td>{{$alert_contact->email}}</td>
                                                <td>{{$alert_contact->phone_number}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <h3 class="title-5 m-b-35">Cameras</h3>
                                </div>
                                <div class="table-data__tool-right">
                                    <a href="{{route('cameras.create')}}" class="au-btn au-btn-icon au-btn--blue2 au-btn--small">
                                        <i class="zmdi zmdi-plus"></i>add camera
                                    </a>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>Camera link</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data->cameras as $camera)
                                            <tr>
                                                <td><a href="{{$camera->address}}">{{$camera->name}}</a></td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <a href="{{route('cameras.edit', $camera->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                        </a>
                                                        <form action="{{route('cameras.destroy', $camera->id)}}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <i class="zmdi zmdi-delete"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="container">
                            <h3 class="title-5 m-b-35">Raised alerts</h3>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data3 datatable">
                                    <thead>
                                        <tr>
                                            <th>Date and time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data->alerts as $alert)
                                            <tr>
                                                <td>
                                                    <a href="{{route('alerts.show',$alert->id)}}" data-toggle="tooltip" data-placement="top" title="View details">
                                                        {{$alert->created_at}}
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
