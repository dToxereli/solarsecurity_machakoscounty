@extends('layouts.admin')

@section('admincontent')
    <div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row m-t-30">
                    <div class="col-md-12">
                        <!-- DATA TABLE-->
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <h3 class="title-5 m-b-35">Alert contacts</h3>
                            </div>
                            <div class="table-data__tool-right">
                                <a href="{{route('alertcontacts.create')}}" class="btn au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i class="zmdi zmdi-plus"></i>Add Alert Contact
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive m-b-40">
                            <table class="table table-borderless table-data2 datatable">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>Solar installation</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $alertcontact)
                                        <tr>
                                            <td>{{$alertcontact->name}}</td>
                                            <td>{{$alertcontact->email}}</td>
                                            <td>{{$alertcontact->phone_number}}</td>
                                            <td>
                                                <ul>
                                                    @foreach ($alertcontact->solar_installations as $solar_installation)
                                                        <li>
                                                            <a href="{{route('solarinstallations.show',$solar_installation->id)}}">{{$solar_installation->name}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <a href="{{route('alertcontacts.edit', $alertcontact->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </a>
                                                    <form action="{{route('alertcontacts.destroy', $alertcontact->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
