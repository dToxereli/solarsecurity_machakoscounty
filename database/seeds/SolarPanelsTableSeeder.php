<?php

use App\SolarPanel;
use Illuminate\Database\Seeder;

class SolarPanelsTableSeeder extends Seeder
{
    public function run()
    {
        factory(SolarPanel::class,10)->create();
}
}
