<?php

use App\Camera;
use Illuminate\Database\Seeder;

class CamerasTableSeeder extends Seeder
{
    public function run()
    {
        factory(Camera::class,10)->create();
}
}
