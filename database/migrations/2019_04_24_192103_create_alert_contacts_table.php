<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertContactsTable extends Migration
{
    public function up()
    {
        Schema::create('alert_contacts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->string('email',80)->nullable()->unique();
            $table->string('phone_number',15)->unique();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alert_contacts');
    }
}
