<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetupRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_contacts_solar_installations', function (Blueprint $table) {
            $table->bigInteger('alert_contact_id')->unsigned()->nullable();    
            $table->bigInteger('solar_installation_id')->unsigned()->nullable();    
        });

        Schema::table('alert_contacts_solar_installations', function (Blueprint $table) {
            $table->foreign('solar_installation_id')->references('id')->on('solar_installations')->onDelete('cascade');
            $table->foreign('alert_contact_id')->references('id')->on('alert_contacts')->onDelete('cascade');
        });

        Schema::table('solar_panels', function (Blueprint $table) {
            $table->foreign('solar_installation_id')->references('id')->on('solar_installations')->onDelete('cascade');
        });

        Schema::table('cameras', function (Blueprint $table) {
            $table->foreign('solar_installation_id')->references('id')->on('solar_installations')->onDelete('cascade');
        });

        Schema::table('alerts', function (Blueprint $table) {
            $table->foreign('solar_installation_id')->references('id')->on('solar_installations')->onDelete('set null');
        });
        
        Schema::table('images', function (Blueprint $table) {
            $table->foreign('alert_id')->references('id')->on('alerts')->onDelete('set null');
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->foreign('alert_id')->references('id')->on('alerts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alert_contacts', function (Blueprint $table) {
            $table->dropForeign(['solar_installation_id']);
        });

        Schema::table('solar_panels', function (Blueprint $table) {
            $table->dropForeign(['solar_installation_id']);
        });

        Schema::table('cameras', function (Blueprint $table) {
            $table->dropForeign(['solar_installation_id']);
        });

        Schema::table('alerts', function (Blueprint $table) {
            $table->dropForeign(['solar_installation_id']);
        });
        
        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign(['alert_id']);
        });

        Schema::table('videos', function (Blueprint $table) {
            $table->dropForeign(['alert_id']);
        });
    }
}
