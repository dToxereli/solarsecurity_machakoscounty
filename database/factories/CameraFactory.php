<?php

use App\Camera;
use Faker\Generator as Faker;

$factory->define(Camera::class, function (Faker $faker) {
return [
'key' => Str::slug($faker->sentence),
'value' => $faker->sentence,
];
});
