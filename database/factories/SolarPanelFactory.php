<?php

use App\SolarPanel;
use Faker\Generator as Faker;

$factory->define(SolarPanel::class, function (Faker $faker) {
return [
'key' => Str::slug($faker->sentence),
'value' => $faker->sentence,
];
});
