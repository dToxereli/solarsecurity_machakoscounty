<?php

use app\SolarInstallation;
use Faker\Generator as Faker;

$factory->define(SolarInstallation::class, function (Faker $faker) {
    return [
        'key' => Str::slug($faker->sentence),
        'value' => $faker->sentence,
    ];
});
