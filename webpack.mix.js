const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .extract(['chart.js'])
   .js([
      'resources/vendor/slick/slick.min',
      'resources/vendor/wow/wow.min.js',
      'resources/vendor/animsition/animsition.min.js',
      'resources/vendor/bootstrap-progressbar/bootstrap-progressbar.min',
      'resources/vendor/counter-up/jquery.waypoints.min.js',
      'resources/vendor/counter-up/jquery.counterup.min',
      'resources/vendor/circle-progress/circle-progress.min.js',
      'resources/vendor/perfect-scrollbar/perfect-scrollbar.js',
      'resources/vendor/select2/select2.min',
   ], 'public/js/libs.js')
   .js('resources/js/main.js', 'public/js/main.js')
   .sass('resources/sass/app.scss', 'public/css')
   .styles([
      'resources/css/font-face.css',
      'resources/vendor/font-awesome-5/css/fontawesome-all.min.css',
      'resources/vendor/font-awesome-4.7/css/font-awesome.min.css',
      'resources/vendor/mdi-font/css/material-design-iconic-font.min.css',
      'resources/vendor/bootstrap-4.1/bootstrap.min.css',
      'resources/vendor/animsition/animsition.min.css',
      'resources/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css',
      'resources/vendor/wow/animate.css',
      'resources/vendor/css-hamburgers/hamburgers.min.css',
      'resources/vendor/slick/slick.css',
      'resources/vendor/select2/select2.min.css',
      'resources/vendor/perfect-scrollbar/perfect-scrollbar.css',
      'resources/css/theme.css',
   ], 'public/css/style.css');
