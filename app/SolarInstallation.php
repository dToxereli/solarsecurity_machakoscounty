<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolarInstallation extends Model {

    protected $tablename = "solar_installations";

    protected $guarded = [];

    public function getGoogleMapUrlAttribute()
    {
        return "https://www.google.com/maps/search/?api=1&query={$this->attributes['latitude']},{$this->attributes['longitude']}";
    }

    public function alert_contacts()
    {
        return $this->belongsToMany('App\AlertContact', 'alert_contacts_solar_installations', 'solar_installation_id', 'alert_contact_id');
    }

    public function alerts()
    {
        return $this->hasMany('App\Alert', 'solar_installation_id', 'id');
    }

    public function cameras()
    {
        return $this->hasMany('App\Camera', 'solar_installation_id', 'id');
    }
}
