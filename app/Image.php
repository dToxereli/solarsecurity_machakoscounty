<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    protected $tablename = "images";

    protected $guarded = [];

    public function alert()
    {
        return $this->belongsTo('App\Alert','alert_id');
    }
}
