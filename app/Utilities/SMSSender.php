<?php
namespace App\Utilities;

use AfricasTalking\SDK\AfricasTalking;
use Log;

class SMSSender
{
    public static function sendSMS($receipients, $message)
    {
                // Set your app credentials
        $username   = "sandbox";
        $apiKey     = "cb7b6373c62c6cac59b3042214bee9e254124baaf19b1c29ca67ae089bba3ae8";

        // Initialize the SDK
        $AT         = new AfricasTalking($username, $apiKey);

        // Get the SMS service
        $sms        = $AT->sms();

        // Set your shortCode or senderId
        $from       = "5748";

        Log::debug($receipients);
        Log::debug($message);

        // Thats it, hit send and we'll take care of the rest
        $result = $sms->send([
            'to'      => $receipients,
            'message' => $message,
            'from'    => $from
        ]);

        Log::debug($result);
    }
}
