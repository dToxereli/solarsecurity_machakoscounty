<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolarPanel;

class SolarPanelController extends Controller
{
    public function index()
    {
        $data = SolarPanel::all();
        return view('solarpanels.index', compact("data"));
    }

    public function create()
    {
        return view('solarpanels.detail');
    }

    public function store(Request $request)
    {
        $data = SolarPanel::create($request->all());
        return view('solarpanels.show', compact("data"));
    }

    public function show($id)
    {
        $data = SolarPanel::findOrFail($id);
        return view('solarpanels.detail', compact("data"));
    }

    public function edit($id)
    {
        $data = SolarPanel::findOrFail($id);
        return view('solarpanels.detail', compact($data));
    }

    public function update(Request $request, $id)
    {
        $data = SolarPanel::find($id)->update($request->all());
        return view('solarpanels.detail', compact("data"));
    }

    public function destroy($id)
    {
        $row = SolarPanel::findOrFail($id);
        $row->delete();
        $data = SolarPanel::all();
        return view('solarpanels.index', compact("data"));
    }
}
