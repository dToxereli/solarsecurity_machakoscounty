<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('users.index', compact("data"));
    }

    public function create()
    {
        return view('users.detail');
    }

    public function store(Request $request)
    {
        $data = User::create($request->all());
        return view('users.show', compact("data"));
    }

    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('users.detail', compact("data"));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('users.detail', compact($data));
    }

    public function update(Request $request, $id)
    {
        $data = User::find($id)->update($request->all());
        return view('users.detail', compact("data"));
    }

    public function destroy($id)
    {
        $row = User::findOrFail($id);
        $row->delete();
        $data = User::all();
        return view('users.index', compact("data"));
    }
}
