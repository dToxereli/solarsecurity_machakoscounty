<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlertContact;
use App\SolarInstallation;

class AlertContactController extends Controller
{
    public function index()
    {
        $data = AlertContact::all();
        return view('alertcontacts.index', compact("data"));
    }

    public function create()
    {
        $solarinstallations = SolarInstallation::all();
        return view('alertcontacts.create', compact("solarinstallations"));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:80|unique:alert_contacts,email',
            'phone_number' => ['required','string','max:15','unique:alert_contacts,phone_number','regex:/^\+254[0-9]{9}$/'],
            'solar_installation_ids.*' => 'nullable|integer|exists:solar_installations,id',
        ]);
        $data = AlertContact::create($request->only('name','email','phone_number'));
        if (isset($input['solar_installation_ids'])) {
            $data->solar_installations()->attach($input['solar_installation_ids']);
        }
        return redirect()->route('alertcontacts.index');
    }

    public function show($id)
    {
        $data = AlertContact::findOrFail($id);
        return redirect()->route('alertcontacts.index');
    }

    public function edit($id)
    {
        $data = AlertContact::findOrFail($id);
        $solarinstallations = SolarInstallation::all();
        return view('alertcontacts.edit', compact("data","solarinstallations"));
    }

    public function update(Request $request, $id)
    {
        $data = AlertContact::findOrFail($id);
        $input = $request->validate([
            'name' => 'required|string|max:100',
            'email' => 'required|email|max:80|unique:alert_contacts,email,'.$data->id,
            'phone_number' => ['required','string','max:15','unique:alert_contacts,phone_number,'.$data->id,'regex:/^\+254[0-9]{9}$/'],
            'solar_installation_ids.*' => 'nullable|integer|exists:solar_installations,id',
        ]);
        $data->update($request->only('name','email','phone_number'));
        $data->solar_installations()->detach();
        if (isset($input['solar_installation_ids'])) {
            $data->solar_installations()->attach($input['solar_installation_ids']);
        }
        return redirect()->route('alertcontacts.index');
    }

    public function destroy($id)
    {
        $row = AlertContact::findOrFail($id);
        $row->delete();
        return redirect()->route('alertcontacts.index');
    }
}
