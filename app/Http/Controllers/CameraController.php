<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Camera;
use App\SolarInstallation;

class CameraController extends Controller
{
    public function index()
    {
        $data = Camera::all();
        return view('cameras.index', compact("data"));
    }

    public function create()
    {
        $solarinstallations = SolarInstallation::all();
        return view('cameras.create', compact("solarinstallations"));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50',
            'address' => 'required|url',
            'solar_installation_id' => 'required|integer|exists:solar_installations,id',
        ]);
        $data = Camera::create($input);
        return redirect()->route('solarinstallations.show', $input['solar_installation_id']);
    }

    public function show($id)
    {
        $data = Camera::findOrFail($id);
        return view('cameras.show', compact("data"));
    }

    public function edit($id)
    {
        $solarinstallations = SolarInstallation::all();
        $data = Camera::findOrFail($id);
        return view('cameras.edit', compact("data","solarinstallations"));
    }

    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'name' => 'required|string|max:50',
            'address' => 'required|url',
            'solar_installation_id' => 'required|integer|exists:solar_installations,id',
        ]);
        $data = Camera::findOrFail($id)->update($input);
        return redirect()->route('solarinstallations.show', $input['solar_installation_id']);
    }

    public function destroy($id)
    {
        $row = Camera::findOrFail($id);
        $row->delete();
        $data = Camera::all();
        return redirect()->route('solarinstallations.show', $row->solar_installation_id);
    }
}
