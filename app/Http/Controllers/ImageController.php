<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{
    // Web
    public function index()
    {
        $data = Image::all();
        return view('images.index', compact("data"));
    }

    public function show($id)
    {
        $data = Image::findOrFail($id);
        return view('images.detail', compact("data"));
    }

    // API

    public function store(Request $request)
    {
        $input = $request->validate([
            'alert_id' => 'required|integer|exists:alerts,id',
            'image' => 'required|image',
        ]);
        $path = $request->file('image')->store('alert_images','public');
        $data = Image::create([
            'alert_id' => $input['alert_id'],
            'path' => $path,
        ]);
        return response()->json(['msg' => 'Image saved'], 200);
    }
}