<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    // Web
    public function index()
    {
        $data = Video::all();
        return view('videos.index', compact("data"));
    }

    public function show($id)
    {
        $data = Video::findOrFail($id);
        return view('videos.detail', compact("data"));
    }

    // API

    public function store(Request $request)
    {
        $input = $request->validate([
            'alert_id' => 'required|integer|exists:alerts,id',
            'video' => 'required|file|mimetypes:video/avi,video/mp4,video/quicktime,video/x-flv',
        ]);
        $path = $request->file('video')->store('alert_videos','public');
        $data = Video::create([
            'alert_id' => $input['alert_id'],
            'path' => $path,
        ]);
        return response()->json(['msg' => 'Video saved'], 200);
    }
}
