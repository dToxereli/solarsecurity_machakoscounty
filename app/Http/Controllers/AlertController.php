<?php

namespace App\Http\Controllers;

use App\Alert;
use App\AlertContact;
use App\SolarInstallation;
use App\Utilities\SMSSender;
use Illuminate\Http\Request;
use AfricasTalking\SDK\AfricasTalking;

class AlertController extends Controller
{
    // Web
    public function index()
    {
        $data = Alert::all();
        return view('alerts.index', compact("data"));
    }

    public function show($id)
    {
        $data = Alert::findOrFail($id);
        return view('alerts.show', compact("data"));
    }

    // API

    public function store(Request $request)
    {
        $input = $request->validate([
            'solar_installation_id' => 'required|integer|exists:solar_installations,id',
        ]);
        $data = Alert::create($input);
        $contacts = AlertContact::all();
        
        if (isset($data->solar_installation) && $data->solar_installation->alert_contacts->count() > 0) {
            $message = "Breach detected at {$data->solar_installation->name}. View location on map {$data->solar_installation->google_map_url}";
            $receipients = join(',',$data->solar_installation->alert_contacts->pluck('phone_number')->all());
            SMSSender::sendSMS($receipients, $message);
        }

        $installation = SolarInstallation::findOrFail($input['solar_installation_id']);
        $installation->alert_status = true;
        $installation->save();

        return response()->json(['msg' => 'Alert received', 'alert' => $data], 200);
    }

    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'solar_installation_id' => 'required|integer|exists:solar_installations,id',
        ]);
        $data = Alert::find($id)->update($request->all());
        return response()->json(['msg' => 'Alert updated', 'alert' => $data], 200);
    }
}
