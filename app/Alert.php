<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model {

    protected $tablename = "alerts";

    protected $guarded = [];

    public function solar_installation()
    {
        return $this->belongsTo('App\SolarInstallation','solar_installation_id');
    }

    public function images()
    {
        return $this->hasMany('App\Image', 'alert_id', 'id');
    }

    public function videos()
    {
        return $this->hasMany('App\Video', 'alert_id', 'id');
    }
}
