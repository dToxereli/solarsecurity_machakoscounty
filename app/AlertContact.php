<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlertContact extends Model {

    protected $tablename = "alert_contacts";

    protected $fillable = ["name","email","phone_number","solar_installation_id"];

    protected $guarded = [];

    public function solar_installations()
    {
        return $this->belongsToMany('App\SolarInstallation', 'alert_contacts_solar_installations', 'alert_contact_id', 'solar_installation_id');
    }
}
